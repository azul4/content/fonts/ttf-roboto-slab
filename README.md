# ttf-roboto-slab

[![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

Roboto Slab is a slab serif addition to the Roboto type family

https://www.google.com/fonts/specimen/Roboto+Slab

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/fonts/ttf-roboto-slab.git
```
